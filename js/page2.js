/* Page 2 - Problem 1
Use only page2.js and jQuery to accomplish the following tasks on page2.html :

When you click on the Button01, even paragraphs should have green background color.*/
/*eslint-env jquery*/
$("#btn01").on('click', function () {
  $('p').each(function () {
    var id = this.id;
    // only do ids ending in 1 or 3 
    var lastChar = id.slice(-1);
    if (lastChar === '1' || lastChar === '3') {
      $('#' + id).css('background-color', 'green');
    }
  });
});

/*When you click on the Button02, a random paragraph should disappear 
(if you click several times, there should be no paragraph).*/

$("#btn02").on('click', function () {
  // get number of paragraphs in the DOM
  var pCount = $('p').length;
  console.log(pCount);

  if (pCount > 0) {
    // get a random number up to the number of paragraphs
    var del = Math.floor(Math.random() * pCount + 1) - 1;
    console.log(del);

    // delete the paragraph at that number from the DOM
    $('p:eq(' + del + ')').remove();
  }
});

/*
When you click on the Button04, all paragraphs should be in upper case characters. 
*/

// When you click on the Button03, paragraph4 and paragraph1 should exchange their position.
$("#btn03").on('click', function () {
  var p1Txt = $('#paragraph-1-1').text();
  var p4Txt = $('#paragraph-1-4').text();

  $('#paragraph-1-1').text(p4Txt);
  $('#paragraph-1-4').text(p1Txt);
});

///When you click on the Button04, all paragraphs should be in upper case characters.
$("#btn04").on('click', function () {
  $('p').each(function () {
    var id = this.id;
    console.log(id);
    var txt = $('#' + id).text().toUpperCase();
    console.log(txt);
    $('#' + id).text(txt);
  });
});

/* Page 2 - Problem 2
Use only page2.js and jQuery to accomplish the following tasks on page2.html :

When you click on the Button05, the table rows should have alternating background color.*/

$(document).ready(function () {
  $('#btn05').on('click', function () {
    console.log('#btn05 clicked');
    $('tr:nth-last-child(even)').css("background-color", "yellow");
    $('tr:nth-last-child(odd)').css("background-color", "grey");
  });
});

/*When you click on the Button06, two rows with with meaningful content should be added. 
One should become the first row and the other should be come the last row. 
Your table will have 6 questions and answers now.*/

$(document).ready(function () {
  $('#btn06').on('click', function () {
    console.log('#btn06 clicked');
    $("#mytable tr").eq(0).after('<tr><td>What is the capital of Sweden?</td><td>Helsinki</td><td>Is This Right?</td></tr>');
    $("#mytable tr:last").after('<tr><td>What is the capital of Russia?</td><td>Moscow</td><td>Could Be?</td></tr>');

  });
});

/*When you click on the Button07, 
the text in the last column will be replaced with random images provided in this project.*/

$("#btn07").on('click', function () {

  // get array length
  var picArrayLen = pictures.length;
  console.log(picArrayLen);

  // loop through each row and update last column
  $('#mytable tr').each(function () {
    // get a random index 
    var index = Math.floor(Math.random() * picArrayLen + 1) - 1;
    console.log(picArrayLen);
    // add an img tag inside the td tag and set the picture
    var imgTag = "<img height=\'65\' width=\'65\' src=\'images/" + pictures[index] + "' />";
    console.log(imgTag);
    $(this).find('td').eq(2).text("");
    $(this).find('td').eq(2).append(imgTag);
  });
});

/*When you click on the Button08, all answers will be empty.*/
$(document).ready(function () {
  $('#btn08').on('click', function () {
    console.log('#btn08 clicked');
    $("td").eq(2, 5, 8, 11).css("opacity", "0");
  });
});

/* Page 2 - Problem 3
Use only page2.js and jQuery to accomplish the following tasks on page2.html :*/

/*When you click on the Button09, 
the content of the alt attribute should appear beside each image.*/
$(document).ready(function () {
  $('#btn09').on('click', function () {
    console.log('#btn09 clicked');
    var textOne = " forest.jpg";
    var textTwo = " never.jpg";
    var textThree = " semi.jpg";

    $("img[src='images/forest.jpg']").after(textOne);
    $("img[src='images/never.jpg']").after(textTwo);
    $("img[src='images/semi.jpg']").after(textThree);
  });
});

/*When you click on the Button10, paragraph 2 
should be replaced by a random image from images provided in this project.*/

$(document).ready(function () {
  $('#btn10').on('click', function () {
    console.log('#btn10 clicked');
    var images = "/images";
    var randomNum = Math.floor((Math.random() * 100) + 1);
    $('images').attr('src', 'Images/images' + randomNum + '.jpg');
    $("#paragraph-3-2").replaceWith('images');
  });
});

/*When you click on the Button11, 
the size of the first image should be decreased by 50% (each time you click).*/

$(document).ready(function () {
  $('#btn11').click(function () {
    console.log('#btn11 clicked');
    $("img[src='images/forest.jpg']").css({
      "transform": "scale(.5)"
    });
    //I can't make it scale down by 50% on the next click
  });

});
/*When you click on the Button12, 
you will toggle sizes of all images (increasing and decreasing sizes by 50% on each click).
*/
$(document).ready(function () {
  $('#btn12').click(function () {
    console.log('#btn12 clicked');
    $("img[src='images/forest.jpg'], [src ='images/never.jpg'] , [src = 'images/semi.jpg']").css({
      'width': $(this).width() * 5.2950,
      'height': $(this).height() * 14.286
    });

  });
});
//I can't make it scale down by 50% on the next click

/*Page 2 - Problem 4
Use only page2.js and jQuery to accomplish the following tasks on page2.html :*/

/*When you click on the Button13, add to each list item a small random image from the provided images.*/

/*Any time you click on the Button14, the font size of all list items should be increased by 25%.*/

/*When you click on the Button15, swap contents of both lists.*/

/*Any time you click on the Button16, the order of list items shifts down. 
That is, first item becomes the second item, the second item becomes the third item, etc. and the last item becomes the first item. This should work for lists of any size.
*/