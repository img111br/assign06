/*
Page 1 - Problem 1
Use only page1.js and jQuery to accomplish the following tasks on page1.html :

1. When you click on the Button01, 
the first four paragraphs should disappear in a smooth motion.
*/
/*eslint-env jquery*/


/*
Then they slide back but this time the text of the 
first two paragraphs is combined and the text for paragraphs 3 and 4 
are also combined. That is, now you have only two paragraphs.*/

/*It should not matter how many times you click on Button01, 
there will be always the two pairs of combined paragraphs only.*/

$(document).ready(function () {
  $('#btn01').on('click', function () {
    //get number of paragraphs
    var paragraphLength = $('#part1 .part1').length;
    console.log('paragraphLength start:' + paragraphLength);

    $('p:lt(' + paragraphLength + ')').hide(1500, function () {
      //if there are 4 paragraphs, combine into 2 before showing
      if (paragraphLength === 4) {
        var line1 = $('p:eq(0)').text() + ' ' + $('p:eq(1)').text();
        var line2 = $('p:eq(2)').text() + ' ' + $('p:eq(3)').text();

        // remove the 2 extra paragraphs
        $('#paragraph-1-3').remove();
        $('#paragraph-1-4').remove();

        // set the remaining 2 paragraph texts
        $('p:eq(0)').text(line1);
        $('p:eq(1)').text(line2);

        // get new length
        paragraphLength = $('#part1 .part1').length;
      }
      console.log('paragraphLength now: ' + paragraphLength);
      $('p:lt(' + paragraphLength + ')').show(1500);
    });
  });
});

/* Page 1 - Problem 2
Use only page1.js and jQuery to accomplish the following tasks on page1.html :

When your mouse enters the heading Page 1, the heading should slide up and disappear for 0.5 seconds.*/

/*Then if the mouse leaves, the heading should return.*/

/*The above behavior should happen only once after the page is displayed.*/

var didMouseAction = false;

// only attach event handler to first header
$(document).ready(function () {
  $('h2:eq(0)').on("mouseenter", function () {
    console.log('h2:eq(0) clicked');
    if (!didMouseAction) {
      $(this).slideUp(500).delay(500);
      didMouseAction = true;
    }
  });
  
  $('h2:eq(0)').on("mouseleave", function () {
    if (didMouseAction) {
      $(this).slideDown(500);
    }
    });
});

/* Page 1 - Problem 3
Use only page1.js and jQuery to accomplish the following tasks on page1.html :

When you click on the table, the table font size should increase to 24 px. 
This change should be animated and take 2 seconds.

If a mouse leaves the table, the table font size should decrease to 14 px. 
This time, this change should be animated and take 3 seconds. */

$(document).ready(function () {
  $('table:first').on("click", function () {
    console.log('table:first clicked');
    var $myTable1 = $("table").eq(0);
    $myTable1.animate({
      'fontSize': "24px"
    }, 2000);
    $("table").eq(0).on("mouseleave", function () {$myTable1.animate({
      'fontSize': "14px"
    }, 3000);
      });
      });
  });

/* Page 1 - Problem 4
Use only page1.js and jQuery to accomplish the following tasks on page1.html :

When you click on Button09, both the first and the second image should 
slide up in a movement that takes 3 seconds.

Then they should slide back in a movement that takes 2.5 seconds.

However, the result of step 2 should be that both images exchange they positions (swap).

The swap should occur whenever you click on Button09 (i.e. images will be swapping after each click).
*/

$(document).ready(function () {
  $("#btn09").on('click', function () {
    var $firstImage = $('IMG').eq(0);
    var $secondImage = $('IMG').eq(1);
    $firstImage.slideUp(3000)
      .queue(function () {
        $(this).attr("src", "images/never.jpg").dequeue()
          .slideDown(2500);
      });
    $secondImage.slideUp(3000)
      .queue(function () {
        $(this).attr("src", "images/forest.jpg").dequeue()
          .slideDown(2500);

      });
  });
});