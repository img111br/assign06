/*Page 3 - Problem 1
Use only page3.js and jQuery to accomplish the following tasks on page3.html :

When page3.html is displayed the first image should start to move repeatedly to the right border and back.  
Clicking on Button09 should work as an ON/OFF switch. That is it should stop or restart the motion.
 

Page 3 - Problem 2
Use only page3.js and jQuery to accomplish the following tasks on page3.html :

When page3.html is displayed the last image should work as a gallery for all pictures stored in the images folder.
An image should disappear in 2.5 seconds and then reappear back in 2.5 seconds but this time it is the next image specified in the pictures array.
The new image should stay for 1 second and then the step 2 is repeated. */